﻿using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SudukoPuzzleMaker : PuzzleMaker {

    public enum Difficulties
    {
       None, VeryEasy, Easy, Medium, Hard, VeryHard
    }

    [System.Serializable]
    public struct Difficulty
    {
        [SerializeField]
        private Difficulties _difficulty;

        [SerializeField]
        private int _lowerBounds;
        [SerializeField]
        private int _upperBounds;
        [SerializeField]
        private int _lowerRownColumnsBounds;

        public Difficulties difficulty { get { return _difficulty; } }
        public int lowerBounds { get { return _lowerBounds; } }
        public int upperBounds { get { return _upperBounds; } }
        public int lowerRnCBounds { get { return _lowerRownColumnsBounds; } }
    }

    //Window Fields
    private Difficulties _currentDifficulty;

    public Difficulty[] _difficulties = new Difficulty[5];
    public Sprite[] _numberSprites = new Sprite[9];

    private List<SudokuCellObject.SudokuCell> _cells = new List<SudokuCellObject.SudokuCell>();

    private int _remainingCount;

    [MenuItem("Tools/Sudoku Maker")]
    public static void ShowWindow()
    {
        GetWindow<SudukoPuzzleMaker>("Sudoku Maker");
    }

    protected override void InitialiseOtherVariables()
    {
        _currentDifficulty = (Difficulties)EditorGUILayout.EnumPopup("Difficulty", _currentDifficulty);

        ScriptableObject target = this;
        SerializedObject sO = new SerializedObject(target);

        SerializedProperty difficultiesProperty = sO.FindProperty("_difficulties");
        SerializedProperty spriteProperty = sO.FindProperty("_numberSprites");

        EditorGUILayout.PropertyField(difficultiesProperty, true);
        EditorGUILayout.PropertyField(spriteProperty, true);
        sO.ApplyModifiedProperties();
    }

    protected override void PuzzleGeneration()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Generate Sudoku"))
            GenerateSudoku();

        if (GUILayout.Button("Toggle Editable"))
            ToggleEditable();

        GUILayout.EndHorizontal();
    }

    protected override void Clear()
    {
        foreach (SudokuCellObject.SudokuCell cell in _cells)
        {
            cell.Value = 0;
            SpriteRenderer child0Renderer = cell.GObject.transform.GetChild(0).GetComponent<SpriteRenderer>();
            child0Renderer.gameObject.SetActive(true);
            child0Renderer.sprite = null;
            child0Renderer.color = Color.white;

            SpriteRenderer child1Renderer = cell.GObject.transform.GetChild(1).GetComponent<SpriteRenderer>();
            child1Renderer.enabled = false;
            child1Renderer.color = Color.white;
        }
    }

    /// <summary>
    /// Adds the necessary information to the Puzzle file
    /// </summary>
    protected override void AddToFile()
    {
        string path = Application.dataPath + "/Resources/SudokuPuzzles.txt";

        int lastid = 0;
        GetID(path, ref lastid);

        string sudokuString = "[";
        string editableBinaryString = "[";

        //Passes the value of a cell and whether it is editable
        foreach (SudokuCellObject.SudokuCell cell in _cells)
        {
            sudokuString += cell.Value.ToString();

            if (!cell.Editable) editableBinaryString += 0.ToString();
            else editableBinaryString += 1.ToString();
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(lastid.ToString() + "-" + _remainingCount.ToString() + " ");
        stringBuilder.Append(sudokuString + " ");
        stringBuilder.Append(editableBinaryString);

        FileAppendPuzzle(path, stringBuilder);
    }

    //Disables/Enables the editable cells
    void ToggleEditable()
    {
        foreach (SudokuCellObject.SudokuCell cell in _cells)
        {
            if (cell.Editable)
                cell.GObject.transform.GetChild(0).gameObject.SetActive(!cell.GObject.transform.GetChild(0).gameObject.activeSelf);
        }
    }

    //Creates Sudoku puzzle
    void GenerateSudoku()
    {
        bool foundValidGrid = false;
        //Until a valid Sudoku has been generated
        while (!foundValidGrid)
        {
            _cells.Clear();
            _cells = new List<SudokuCellObject.SudokuCell>();
            foreach (Transform child in _gridObject.transform)
            {
                child.GetComponent<SpriteRenderer>().color = Color.white;
                child.transform.GetChild(0).gameObject.SetActive(true);
                SudokuCellObject.SudokuCell cell = child.GetComponent<SudokuCellObject>().CellInformation;
                if (cell != null)
                {
                    cell.Value = 0;
                    cell.Editable = true;
                    _cells.Add(cell);
                }
            }

            //Create Sudoku
            BacktrackingAlgorithm btAlgorithm = new BacktrackingAlgorithm(_cells);
            btAlgorithm.Apply();

            Difficulty difficulty = new Difficulty();

            foreach (Difficulty diff in _difficulties)
            {
                if (diff.difficulty == _currentDifficulty)
                {
                    difficulty = diff;
                    break;
                }
            }

            if (difficulty.difficulty == Difficulties.None) return;

            //Dig holes
            DiggingHoles dHoles = new DiggingHoles(_cells, new DiggingHoles.Bounds(difficulty.lowerBounds, difficulty.upperBounds, difficulty.lowerRnCBounds));
            foundValidGrid = dHoles.DigSequence();
            _remainingCount = dHoles._randomRemainValue;
        }

        foreach (SudokuCellObject.SudokuCell cell in _cells)
            if (cell.Value != 0) cell.GObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = _numberSprites[cell.Value - 1];
    }

    private static SudokuCellObject.SudokuCell GetCell(List<SudokuCellObject.SudokuCell> cells, int column, int row)
    {
        return cells.Find(s => s.Column == column && s.Row == row);
    }
}
