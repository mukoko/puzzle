﻿using UnityEngine;
using UnityEditor;

public class SudokuGridMaker : GridMaker {
    private Sprite _blank0;
    private Sprite _blank1;

    //Opens the Window
    [MenuItem("Tools/Sudoku Grid Maker")]
    public static void ShowWindow()
    {
        GetWindow<SudokuGridMaker>("Sudoku Grid Maker");
    }

    //Initialise additional variables that are not found in the parent class
    protected override void InitialiseOtherVariables()
    {
        _blank0 = (Sprite)EditorGUILayout.ObjectField("Sprite 0", _blank0, typeof(Sprite), true);
        _blank1 = (Sprite)EditorGUILayout.ObjectField("Sprite 1", _blank1, typeof(Sprite), true);
    }

    //Called by each cell created by the grid maker
    protected override void InitialiseCell(GameObject obj, int column, int row)
    {
        //Separates cells into different regions
        int region = 0;
        if (row <= 3)
        {
            if (column <= 3)
                region = 1;
            else if (column <= 6)
                region = 2;
            else if (column <= 9)
                region = 3;
        }
        else if (row <= 6)
        {
            if (column <= 3)
                region = 4;
            else if (column <= 6)
                region = 5;
            else if (column <= 9)
                region = 6;
        }
        else if (row <= 9)
        {
            if (column <= 3)
                region = 7;
            else if (column <= 6)
                region = 8;
            else if (column <= 9)
                region = 9;
        }

        //Draws different sprites to the renderer to differentiate the regions
        SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
        if (region % 2 == 0) renderer.sprite = _blank1;
        else renderer.sprite = _blank0;

        obj.GetComponent<SudokuCellObject>().Initialise(column, row, region);
    }
}