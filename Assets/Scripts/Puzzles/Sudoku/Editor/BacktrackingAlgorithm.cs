﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// In summary, this algorithm creates a random valid sudoku by randomly entering a number into a cell, if the number violates the rules
/// of a sudoku then another is choosen if no numbers work then the algorithm backtracks to the previous cell, changing its number
/// </summary>
public class BacktrackingAlgorithm
{
    /// <summary>
    /// This struct. records all of the numbers that have been attempted in a cell, to stop the same numbers being continuously choosen
    /// </summary>
    private struct CellValueTracker
    {
        public SudokuCellObject.SudokuCell Cell { get; private set; }
        public List<int> AvaliableValues { get; private set; }

        public CellValueTracker(SudokuCellObject.SudokuCell cell)
        {
            Cell = cell;
            AvaliableValues = new List<int>();
            ResetValues();
        }

        public void ResetValues()
        {
            AvaliableValues.Clear();
            for (int i = 1; i <= 9; i++)
                AvaliableValues.Add(i);
        }
    }

    public List<SudokuCellObject.SudokuCell> SudokuCells { get; private set; }

    private List<CellValueTracker> _valueTrackers;
    private int _currentSqr;

    public BacktrackingAlgorithm(List<SudokuCellObject.SudokuCell> cells)
    {
        SudokuCells = cells;
        _valueTrackers = new List<CellValueTracker>();

        foreach (SudokuCellObject.SudokuCell cell in SudokuCells)
            _valueTrackers.Add(new CellValueTracker(cell));
    }


    /// <summary>
    /// Prevent the algorithm from assiging a value to a certain cell
    /// </summary>
    public void RestrictValue(SudokuCellObject.SudokuCell cell, int value)
    {
        CellValueTracker cTracker = GetTracker(cell);
        cTracker.AvaliableValues.Remove(value);
    }

    public bool Apply()
    {
        //Until all 81 cells have been filled the following sequence should be repeated
        while (_currentSqr < 81)
        {
            //Retrieves the cell at index _currentSqr
            CellValueTracker cTracker = GetTracker(SudokuCells[_currentSqr]);

            //A cell marked as non-editable cannot have its value changed
            if (!cTracker.Cell.Editable)
            {
                //So we just skip the cell
                _currentSqr += 1;
                continue;
            }

            //If there are still values to try then try them
            if (cTracker.AvaliableValues.Count != 0)
            {
                int proposedValue = GetRandom(cTracker.AvaliableValues);
                cTracker.AvaliableValues.Remove(proposedValue);

                //Checks if placing the number in the cell will violate the rules of sudoku
                if (!Conflict(SudokuCells, cTracker.Cell, proposedValue))
                {
                    //Found a valid value for the cell
                    cTracker.Cell.Value = proposedValue;
                    _currentSqr += 1;
                }
            }
            else
            {
                //If there a no avaliable values to try then we should backtrack to the previous editable cell
                cTracker.ResetValues();
                if (_currentSqr > 0)
                {
                    //None of the previous cells are editable, a valid sudoku cannot be made
                    if (!FoundPrevEditableCell())
                    {
                        Debug.Log("No output");
                        break;
                    }
                }
                else
                {
                    //We cannot backtrack any further, there is no valid cell, with the given non-editable cell values
                    Debug.Log("No output");
                    break;
                }
            }
        }

        return _currentSqr == 81;
    }

    /// <summary>
    /// Checks whether a plugging a value in a certain cell will violate the rules of Sudoku
    /// </summary>
    /// <returns></returns>
    public static bool Conflict(List<SudokuCellObject.SudokuCell> cells, SudokuCellObject.SudokuCell cell, int proposedValue)
    {
        foreach (SudokuCellObject.SudokuCell sCell in cells)
        {
            if (sCell != cell)
            {
                //A cell value is valid, if no other cell in the same row/column/region(square) has the same value
                if (sCell.Row == cell.Row || sCell.Column == cell.Column || sCell.Region == cell.Region)
                {
                    if (sCell.Value == proposedValue) return true;
                }
            }
        }

        return false;
    }

    private CellValueTracker GetTracker(SudokuCellObject.SudokuCell cell)
    {
        return _valueTrackers.Find(s => s.Cell == cell);
    }

    private int GetRandom(List<int> avaliableNumbers)
    {
        int index = Random.Range(0, avaliableNumbers.Count);
        return avaliableNumbers[index];
    }

    /// <summary>
    /// Attemots to find the previous non-editable cell in the Sudoku grid
    /// </summary>
    private bool FoundPrevEditableCell()
    {
        bool found = false;
        while (!found)
        {
            _currentSqr -= 1;
            //Found the previous editable cell
            if (SudokuCells[_currentSqr].Editable)
            {
                found = true;
                //Reset it's values
                SudokuCells[_currentSqr].Value = 0;
            }
            else if (_currentSqr == 0)
                break;
        }

        return found;
    }
}


