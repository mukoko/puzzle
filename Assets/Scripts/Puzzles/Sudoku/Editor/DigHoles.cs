﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class takes a Sudoku and begins to plug holes into it until the current puzzle is no longer unique, or fails requirements
/// </summary>
public class DiggingHoles {
    public struct Bounds
    {
        public Bounds(int lb, int ub, int rncb)
        {
            NoOfGivensLB = lb;
            NoOfGivensUB = ub;
            NoOfGivensRnCLB = rncb;
        }

        public int NoOfGivensLB { get; private set; }
        public int NoOfGivensUB { get; private set; }
        public int NoOfGivensRnCLB { get; private set; }
    }

    public int _randomRemainValue;

    private List<SudokuCellObject.SudokuCell> _cells;
    private List<SudokuCellObject.SudokuCell> _sequencedCells;
    private Bounds _boundaries;

    private int _currentIndex;
    private int _noRemaining = 81;

    public DiggingHoles(List<SudokuCellObject.SudokuCell> cells, Bounds difficulty)
    {
        _cells = cells;
        foreach (SudokuCellObject.SudokuCell cell in _cells)
            cell.Editable = false;

        _boundaries = difficulty;
        //Randomizes the order that the Cells in the Sudoku list (cells) is ordered
        GlobalRandomization(out _sequencedCells);
        _randomRemainValue = Random.Range(_boundaries.NoOfGivensLB, _boundaries.NoOfGivensUB + 1);
    }

    public bool DigSequence()
    {
        //Will attempt to dig a hole at every cell in the grid
        while (_currentIndex < 81)
        {
            SudokuCellObject.SudokuCell currentCell = _sequencedCells[_currentIndex];
            _currentIndex++;

            //After getting the current cell at index _currentIndex, it checks if the removing the cell will violate the restrictions on removing cells.
            //These are that each column and row must have a given number of remaining cells, and overall there must be a given number of cells left across the board
            if (!CheckViolations(currentCell)) continue;

            //Creates a copy of the Sudoku
            List<SudokuCellObject.SudokuCell> copy = new List<SudokuCellObject.SudokuCell>();
            SudokuCellObject.SudokuCell copyCell = null;
            foreach (SudokuCellObject.SudokuCell cell in _cells)
            {
                SudokuCellObject.SudokuCell s = new SudokuCellObject.SudokuCell(cell.Column, cell.Row, cell.Region);
                s.Value = cell.Value;
                s.Editable = cell.Editable;

                if (cell.Row == currentCell.Row && cell.Column == currentCell.Column)
                    copyCell = s;

                copy.Add(s);
            }

            if (copyCell == null) continue;

            //All of the cells in the copy are set to non-editable
            //The backtracking algorithm is performed on the copy
            BacktrackingAlgorithm btAlgo = new BacktrackingAlgorithm(copy);
            //Only the current cell and any other cell that was also removed is editable
            
            //The aim of performing this algorithm is to restrict the value allowed in the current cell, and see if another puzzle can be 
            //created using another value. If so the sudoku is not unique
            btAlgo.RestrictValue(copyCell, copyCell.Value);
            copyCell.Editable = true;
            if (!btAlgo.Apply())
            {
                //No other solutions so removing that value will still have a puzzle with only one solution
                currentCell.Editable = true;
                _noRemaining--;
            }
        }

        Debug.Log(_randomRemainValue);
        Debug.Log(_noRemaining);
        return _randomRemainValue == _noRemaining;
    }

    /// <summary>
    /// Checks that carving out the cell's value will not violate the requirements set on the number of cells that should remain in
    /// a row, column, whole puzzle
    /// </summary>
    private bool CheckViolations(SudokuCellObject.SudokuCell cell)
    {
        int columnRemainCount = 0;
        int rowRemainCount = 0;
        int tempRemaining = _noRemaining - 1;

        foreach (SudokuCellObject.SudokuCell c in _cells)
        {
            if (c == cell) continue;

            if (c.Column == cell.Column && !c.Editable)
                columnRemainCount++;

            if (c.Row == cell.Row && !c.Editable)
                rowRemainCount++;
        }

        if (columnRemainCount < _boundaries.NoOfGivensRnCLB || rowRemainCount < _boundaries.NoOfGivensRnCLB || tempRemaining < _randomRemainValue) return false;
        return true;
    }

    //Randomize the order of the list
    private void GlobalRandomization(out List<SudokuCellObject.SudokuCell> cells)
    {
        cells = new List<SudokuCellObject.SudokuCell>();
        cells.AddRange(_cells);

        int n = cells.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n);
            SudokuCellObject.SudokuCell s = cells[k];
            cells[k] = cells[n];
            cells[n] = s;
        }
    }
}