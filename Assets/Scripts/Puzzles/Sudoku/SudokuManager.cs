﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class SudokuManager : PuzzleManager {
    [SerializeField]
    private GameObject _optionsHolder;

    [SerializeField]
    private Color _highlightCellColor;

    [SerializeField]
    private Color _numberErrorColor;

    [SerializeField]
    private Sprite[] _numberSprites;


    private List<SudokuCellObject.SudokuCell> _cells;
    private SudokuCellObject.SudokuCell _currentCell;
    private bool _validNoInput;

    private GameObject _selectedNumberObject;

    protected override void Initialise(int puzzleID)
    {
        base.Initialise(puzzleID);
        SetCellInformation();
    }

    /// <summary>
    /// Retrieves the necessary information regarding a puzzle with a given ID.
    /// </summary>
    protected override void GetPuzzle()
    {
        TextAsset file = Resources.Load("SudokuPuzzles") as TextAsset;
        string[] lines = file.text.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

        string sudokuPuzzleInfo = GetPuzzleInformation(lines, _puzzleID, '-');
        if (sudokuPuzzleInfo != null)
        {
            int indexOfFirstSpace = sudokuPuzzleInfo.IndexOf(" ");
            int indexOfSecondSpace = sudokuPuzzleInfo.LastIndexOf(" ");

            _puzzleInformation = new string[2];
            //The values of each cell
            _puzzleInformation[0] = sudokuPuzzleInfo.Substring(indexOfFirstSpace + 1, indexOfSecondSpace - 1 - indexOfFirstSpace);
            //Whether a cell is editable (0 = no, 1 = yes)
            _puzzleInformation[1] = sudokuPuzzleInfo.Substring(indexOfSecondSpace + 1);
        }
    }

    /// <summary>
    /// Sets up the Sudoku grid
    /// </summary>
    private void SetCellInformation()
    {
        _cells = new List<SudokuCellObject.SudokuCell>();
        int currentPosition = 1;

        foreach (Transform child in _gridObject.transform)
        {
            int value = int.Parse(_puzzleInformation[0][currentPosition].ToString());
            int editable = int.Parse(_puzzleInformation[1][currentPosition].ToString());
            currentPosition++;

            SudokuCellObject.SudokuCell cell = child.GetComponent<SudokuCellObject>().CellInformation;
            _cells.Add(cell);

            cell.Editable = true;
            if (editable == 0)
            {
                cell.GObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = _numberSprites[value - 1];
                cell.Value = value;
                cell.Editable = false;
            }
        }
    }

    protected override void Update()
    {
        if (_completed) return;
        base.Update();

        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D[] collidingObjects = Physics2D.OverlapPointAll(mousePosition);

            if (collidingObjects != null)
                ScreenPressed(collidingObjects);
        }

        if (Input.GetMouseButtonUp(0) && _selectedNumberObject != null)
            SelectNumberOption(_selectedNumberObject);
    }

    /// <summary>
    /// Called whenever the MouseButton(0) is pressed on either a cell, or a number option button
    /// </summary>
    void ScreenPressed(Collider2D[] collidingObjects)
    {
        //Returns the first object that the mouse clicks on
        if (collidingObjects.Length == 1 && collidingObjects[0] != null)
        {
            //If the object is a grid cell, then select the cell
            if (collidingObjects[0].transform.root.gameObject == _gridObject)
                SelectCell(collidingObjects[0].gameObject);

            //If the cell was a number option & a cell has been selected, then the number will be placed into that cell
            if (collidingObjects[0].transform.root.gameObject == _optionsHolder)
            {
                _selectedNumberObject = collidingObjects[0].gameObject;
                _selectedNumberObject.GetComponent<SpriteRenderer>().color = _highlightCellColor;
                if (_currentCell != null) _validNoInput = true;
            }
        }
    }

    void SelectCell(GameObject cObject)
    {
        SudokuCellObject.SudokuCell sudokuCell = cObject.GetComponent<SudokuCellObject>().CellInformation;
        SpriteRenderer highlightRenderer = cObject.transform.GetChild(1).GetComponent<SpriteRenderer>();

        //If you click on the cell that was already selected
        if (_currentCell == sudokuCell)
        {
            //Then de-select it
            _currentCell = null;
            foreach (SudokuCellObject.SudokuCell cell in _cells)
            {
                //Return all the highlighted cells back to normal
                SpriteRenderer iterCellRenderer = cell.GObject.transform.GetChild(1).GetComponent<SpriteRenderer>();
                iterCellRenderer.color = Color.white;
                iterCellRenderer.enabled = false;
            }

            return;
        }

        //Else if the cell wasn't selected, then we select it
        _currentCell = sudokuCell;
        highlightRenderer.color = _highlightCellColor;
        highlightRenderer.enabled = true;

        //If the cell is in the same row, column or region, then we highlight as an indicator
        foreach (SudokuCellObject.SudokuCell cell in _cells)
        {
            if (cell == _currentCell) continue;

            SpriteRenderer iterCellRenderer = cell.GObject.transform.GetChild(1).GetComponent<SpriteRenderer>();
            iterCellRenderer.color = Color.white;
            iterCellRenderer.enabled = false;

            if (cell.Column == sudokuCell.Column)
                iterCellRenderer.enabled = true;

            if (cell.Row == sudokuCell.Row)
                iterCellRenderer.enabled = true;

            if (cell.Region == sudokuCell.Region)
                iterCellRenderer.enabled = true;
        }
    }

    //Called once we have released the MouseButton(0) after clicking on a number option
    void SelectNumberOption(GameObject optionObject)
    {
        _selectedNumberObject = null;
        //Sets the color back to normal rather than the button select color
        optionObject.GetComponent<SpriteRenderer>().color = Color.white;

        //We do not want to proceed with placing the number into the select cell if
        //The current cell is null or the cell is not editable
        if (_validNoInput == false) return;
        if (_currentCell == null) return;
        if (_currentCell.Editable == false) return;

        _validNoInput = false;

        //Gets the value and sprite to place from the option object
        int value = int.Parse(optionObject.name);
        Sprite sprite = optionObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;

        SpriteRenderer numberRenderer = _currentCell.GObject.transform.GetChild(0).GetComponent<SpriteRenderer>();
        numberRenderer.color = Color.white;

        //Places necessary number/sprite
        if (_currentCell.Value == value)
        {
            _currentCell.Value = 0;
            numberRenderer.sprite = null;
        }
        else
        {
            _currentCell.Value = value;
            numberRenderer.sprite = sprite;
        }

        foreach (SudokuCellObject.SudokuCell cell in _cells)
        {
            if (cell == _currentCell) continue;

            if (cell.Column == _currentCell.Column || cell.Row == _currentCell.Row || cell.Region == _currentCell.Region)
            {
                SpriteRenderer cellNoRenderer = cell.GObject.transform.GetChild(0).GetComponent<SpriteRenderer>();
                //We check for conflicts regarding the newly placed number
                if (cell.Value == _currentCell.Value)
                {
                    if (_currentCell.Value != 0)
                    {
                        numberRenderer.color = _numberErrorColor;
                        cellNoRenderer.color = _numberErrorColor;
                    }
                }
                //We check for conflicts that cells in the same row, column, region may have with other numbers
                else if (!HasOtherConflicts(cell))
                    cellNoRenderer.color = Color.white;
            }
        }

        CheckCompletion();
    }

    /// <summary>
    /// Returns true if the cell c, doesn't have the same value as another cell in the same row, column or region
    /// </summary>
    bool HasOtherConflicts(SudokuCellObject.SudokuCell c)
    {
        foreach (SudokuCellObject.SudokuCell cell in _cells)
        {
            if (cell == c) continue;
            if (cell.Column == c.Column || cell.Row == c.Row || cell.Region == c.Region)
            {
                if (cell.Value == c.Value) return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Adds all of the values in each cell to an array, then checks if it is equal to the array that holds all of the cells complete values
    /// </summary>
    protected override void CheckCompletion()
    {
        int[] currentSudokuArray = new int[81];
        int currentPosition = 0;
        foreach (SudokuCellObject.SudokuCell cell in _cells)
        {
            currentSudokuArray[currentPosition] = cell.Value;
            currentPosition++;
        }

        string sudokuArrayString = "[";
        foreach (int i in currentSudokuArray)
            sudokuArrayString += i.ToString();
        sudokuArrayString += "]";

        if (sudokuArrayString.Equals(_puzzleInformation[0]))
        {
            SetComplete(Application.streamingAssetsPath + "/SudokuScores.txt", '-');
        }
    }
}
