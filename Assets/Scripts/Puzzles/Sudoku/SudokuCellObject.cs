﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SudokuCellObject : MonoBehaviour {

    [SerializeField]
    private SudokuCell _sudokuCell;
    public SudokuCell CellInformation { get { return _sudokuCell; } }

    public void Initialise(int column, int row, int region)
    {
        _sudokuCell = new SudokuCell(column, row, region, gameObject);
    }

    [System.Serializable]
    public class SudokuCell
    {
        [SerializeField]
        private int _column;

        [SerializeField]
        private int _row;

        [SerializeField]
        private int _region;

        [SerializeField]
        private GameObject _gObj;

        public int Column { get { return _column; } }
        public int Row { get { return _row; } }
        public int Region { get { return _region; } }
        public GameObject GObject { get { return _gObj; } }
        public int Value { get; set; }
        public bool Editable { get; set; }

        public SudokuCell(int c, int r, int region, GameObject obj = null)
        {
            _column = c;
            _row = r;
            _region = region;
            _gObj = obj;
        }
    }
}
