﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum PuzzleType
{
    Sudoku = 1, Block = 2, Shuffle = 3
}
public abstract class PuzzleManager : MonoBehaviour {

    [SerializeField]
    private PuzzleType _puzzleType;

    [SerializeField]
    protected GameObject _gridObject;

    [SerializeField]
    private GameObject _completePanel;

    [SerializeField]
    private GameObject _nextLevelButton;

    [SerializeField]
    private Text _puzzleText;

    [SerializeField]
    private Text _timerText;

    [SerializeField]
    private Text _currentTime;

    [SerializeField]
    private Text _recordTime;

    private float _timer;
    private string _minutes;
    private string _seconds;

    protected bool _completed;

    protected int _puzzleID;
    protected string[] _puzzleInformation;

    private int _lastID;

	protected virtual void Awake()
    {
        Initialise(PlayerPrefs.GetInt(_puzzleType.ToString() + "PuzzleID", 1));
    }

    protected virtual void Initialise(int puzzleID)
    {
        _puzzleID = puzzleID;
        _puzzleText.text = _puzzleType.ToString() + " " + _puzzleID;

        GetPuzzle();
    }

    protected virtual void Update()
    {
        _timer += Time.deltaTime;
        _minutes = Mathf.Floor(_timer / 60).ToString("00");
        _seconds = (_timer % 60).ToString("00");
        _timerText.text = _minutes + ":" + _seconds;

        if (Input.GetKeyDown(KeyCode.N))
            NextLevel();
        if (Input.GetKeyDown(KeyCode.P))
            PreviousLevel();
    }

    /// <summary>
    /// Finds the necessary information for a puzzle based on a given ID
    /// </summary>
    protected string GetPuzzleInformation(string[] fileLines, int id, char delimiter)
    {
        string info = null;
        _lastID = 0;

        foreach (string line in fileLines)
        {
            int indexOfDelimiter = line.IndexOf(delimiter);
            int lineID = _lastID = int.Parse(line.Substring(0, indexOfDelimiter));
            if (lineID == id)
                info = line;
        }

        //The lastID is the last id in the puzzle file
        //If the puzzle is the last in the file destroy the next level button
        if (id == _lastID)
            Destroy(_nextLevelButton);
        
        return info;
    }

    protected void SetComplete(string path, char delimiter)
    {
        _completed = true;
        _completePanel.SetActive(true);
        _currentTime.text = "Current Time : " + _minutes + ":" + _seconds;
        PuzzleScore.Save(_puzzleType, _puzzleID, _minutes, _seconds, ref _recordTime);
    }

    protected abstract void GetPuzzle();
    protected abstract void CheckCompletion();

    ////////////////////////////////////////////////////////////////////
    /// <summary>
    /// UI Stuff.
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Exit()
    {
        SceneManager.LoadScene(0);
    }

    public void NextLevel()
    {
        if (_puzzleID != _lastID)
            PlayerPrefs.SetInt(_puzzleType.ToString() + "PuzzleID", _puzzleID + 1);
        Restart();
    }

    public void PreviousLevel()
    {
        if (_puzzleID != 1)
            PlayerPrefs.SetInt(_puzzleType.ToString() + "PuzzleID", _puzzleID - 1);
        Restart();
    }
}
