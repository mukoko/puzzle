﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleCellObject : MonoBehaviour {

    [SerializeField]
    private ShuffleCell _shuffleCell;
    public ShuffleCell CellInformation { get { return _shuffleCell; } }

    public void Initialise(int column, int row)
    {
        _shuffleCell = new ShuffleCell(column, row, gameObject);
    }

    [System.Serializable]
    public class ShuffleCell
    {
        [SerializeField]
        private int _column;

        [SerializeField]
        private int _row;

        [SerializeField]
        private GameObject _gObj;

        public int Column { get { return _column; } }
        public int Row { get { return _row; } }
        public GameObject GObject { get { return _gObj; } }
        public bool StaticCell { get; set; }
        public Color CurrentColor { get; set; }

        public ShuffleCell(int column, int row, GameObject gObj)
        {
            _column = column;
            _row = row;
            _gObj = gObj;
        }  
    }
}
