﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ShuffleManager : PuzzleManager {

    [SerializeField]
    private Sprite _normalSprite;

    [SerializeField]
    private Sprite _blockedSprite;

    [SerializeField]
    private Sprite[] _selectionSprites;

    [SerializeField]
    private Text _numberOfMovesRemaining;

    private List<Color> _colors = new List<Color>(7);
    private List<ShuffleCellObject.ShuffleCell> _cells = new List<ShuffleCellObject.ShuffleCell>(49);

    private ShuffleCellObject _selectedCell;
    private ShuffleCellObject _proposedCell;

    private Vector2 _lastMousePosition;

    private int _movesUsed = 0;
    private int _maxMoves;


    protected override void Initialise(int puzzleID)
    {
        base.Initialise(puzzleID);

        GetColors();
        FillGrid();        
    }


    /// <summary>
    /// Each Shuffle puzzle has an entry containing all of the colours used in the puzzle.
    /// This method separates the string holding these colors, and adds them to a list of Color objects.
    /// </summary>
    void GetColors()
    {
        int indexOfFirstColon = _puzzleInformation[2].IndexOf(":");
        for (int i = 0; i < _puzzleInformation[2].Length; i++)
        {
            //Each color is separate by a delimiter ':'
            char c = _puzzleInformation[2][i];
            if (c.Equals(':'))
            {
                //Retrieves the string between the current and previous ':'
                string s = null;
                if (i == indexOfFirstColon) s = _puzzleInformation[2].Substring(1, i - 1);
                else
                {
                    string sub = _puzzleInformation[2].Substring(0, i - 1);
                    int prevColon = sub.LastIndexOf(":");

                    s = _puzzleInformation[2].Substring(prevColon + 1, i - 1 - prevColon);
                }

                if (s != null)
                {
                    //colArray contains the RGB values
                    float[] colArray = new float[3];
                    int index = 0;

                    for (int k = 0; k < s.Length; k++)
                    {
                        //Each color value is separate by a delimiter ','
                        //Get the three values and add it to the array
                        char cK = s[k];
                        if (cK.Equals(',') && index < 3)
                        {
                            float f = float.Parse(s.Substring(k - 5, 5));
                            colArray[index] = f;
                            index++;
                        }
                    }

                    //Add the color to the list
                    _colors.Add(new Color(colArray[0], colArray[1], colArray[2], 1f));
                }
            }
        }

    }

    void FillGrid()
    {
        int currentPosition = 0;
        foreach (Transform child in _gridObject.transform)
        {
            currentPosition++;
            int cellType = int.Parse(_puzzleInformation[0][currentPosition].ToString());

            if (cellType == 0)
            {
                child.gameObject.SetActive(false);
                continue;
            }

            ShuffleCellObject.ShuffleCell cell = child.GetComponent<ShuffleCellObject>().CellInformation;
            _cells.Add(cell);

            SpriteRenderer renderer = child.GetComponent<SpriteRenderer>();

            if (cellType == 1)
            {
                renderer.color = _colors[int.Parse(_puzzleInformation[1][currentPosition].ToString()) - 1];
                cell.CurrentColor = renderer.color;
            }
            else if (cellType == 2)
            {
                renderer.sprite = _blockedSprite;
                cell.StaticCell = true;
            }
        }
    }

    /// <summary>
    /// Get puzzle information
    /// </summary>
    protected override void GetPuzzle()
    {

        TextAsset file = Resources.Load("ShufflePuzzles") as TextAsset;
        string[] lines = file.text.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

        string puzzleInfo = GetPuzzleInformation(lines, _puzzleID, '-');

        if (puzzleInfo != null)
        {
            int indexOfHyphen = puzzleInfo.IndexOf("-");
            int indexOfFirstSpace = puzzleInfo.IndexOf(" ");
            int indexOfSecondSpace = puzzleInfo.IndexOf(" ", puzzleInfo.IndexOf(" ") + 1);
            int indexOfThirdSpace = puzzleInfo.IndexOf(" ", indexOfSecondSpace + 1);

            _maxMoves = int.Parse(puzzleInfo.Substring(indexOfHyphen + 1, indexOfFirstSpace - 1 - indexOfHyphen));
            _numberOfMovesRemaining.text = "0/" + _maxMoves;

            _puzzleInformation = new string[3];
            //Whether a cell is de-active (0), active (1) or static (2)
            _puzzleInformation[0] = puzzleInfo.Substring(indexOfFirstSpace + 1, indexOfSecondSpace - 1 - indexOfFirstSpace);
            //The color index that the cell holds eg: 1 may refer to red
            _puzzleInformation[1] = puzzleInfo.Substring(indexOfSecondSpace + 1, indexOfThirdSpace - 1 - indexOfSecondSpace);
            //The order that the colors are sorted by row: using eg above: first color will be red
            _puzzleInformation[2] = puzzleInfo.Substring(indexOfThirdSpace + 1);
        }
    }


    protected override void Update()
    {
        if (_completed) return;
        base.Update();

        if (Input.GetMouseButtonDown(0))
        {
            _lastMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Retrieve the collider of the gameObject you just pressed
            Collider2D collidingObject = Physics2D.OverlapPoint(_lastMousePosition);

            if (collidingObject != null)
            {
                //If the object is a child of the grid object, then select it
                if (collidingObject.transform.root.gameObject == _gridObject) 
                    SelectCell(collidingObject.gameObject);
            }
        }

        //If a cell has been selected
        if (_selectedCell != null)
        {
            Vector2 currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //And the mouse has moved
            if (currentMousePosition != _lastMousePosition)
            {
                //Get the object you are now hovering over
                Collider2D collidingObject = Physics2D.OverlapPoint(currentMousePosition);
                _lastMousePosition = currentMousePosition;

                if (collidingObject != null)
                {
                    //This new cell should be the proposed cell to swap
                    if (collidingObject.transform.root.gameObject == _gridObject)
                        ProposeNewCell(collidingObject.gameObject);  
                }
            }

            //Once you release the MouseButton(0) whilst a cell is selected
            if (Input.GetMouseButtonUp(0))
            {
                //If no cell has been proposed, put the cell back to the original color
                if (_proposedCell == null)
                {
                    _selectedCell.GetComponent<SpriteRenderer>().color = _selectedCell.CellInformation.CurrentColor;
                    _selectedCell.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
                    _selectedCell = null;
                }
                else
                {
                    //Else swap the colors
                    Color cA = _selectedCell.CellInformation.CurrentColor;
                    Color cB = _proposedCell.CellInformation.CurrentColor;

                    ChangeColor(ref _selectedCell, cB);
                    ChangeColor(ref _proposedCell, cA);

                    _movesUsed++;
                    _numberOfMovesRemaining.text = _movesUsed + "/" + _maxMoves;

                    //Each time you swap a cell check if the puzzle was completed
                    CheckCompletion();

                    if (_movesUsed == _maxMoves && !_completed)
                        Restart();
                }
            }
        }
    }

    void ChangeColor(ref ShuffleCellObject cell, Color color)
    {
        cell.CellInformation.CurrentColor = color;
        cell.GetComponent<SpriteRenderer>().color = color;
        cell.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
        cell = null;
    }

    void SelectCell(GameObject collidingObject)
    {
        ShuffleCellObject cellObject = collidingObject.GetComponent<ShuffleCellObject>();
        if (!cellObject.CellInformation.StaticCell)
        {
            _selectedCell = cellObject;
            SpriteRenderer renderer = _selectedCell.transform.GetChild(0).GetComponent<SpriteRenderer>();
            renderer.sprite = _selectionSprites[0];
        }
    }

    void ProposeNewCell(GameObject collidingObject)
    {
        ShuffleCellObject collidingCell = collidingObject.GetComponent<ShuffleCellObject>();
        //Shouldn't change anything if the colliding cell is the one that has already been proposed
        if (collidingCell != _proposedCell)
        {
            //If you hover other another cell, then remove the current proposed cell
            if (_proposedCell != null)
            {
                _proposedCell.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
                _proposedCell = null;
            }

            //If the cell you hover over is a static one, then the selected cell displays the default selection
            SpriteRenderer sChildRenderer = _selectedCell.transform.GetChild(0).GetComponent<SpriteRenderer>();
            if (collidingCell.CellInformation.StaticCell) sChildRenderer.GetComponent<SpriteRenderer>().sprite = _selectionSprites[0];
            else
            {
                _proposedCell = collidingCell;
                int sColumn = _selectedCell.CellInformation.Column;
                int sRow = _selectedCell.CellInformation.Row;
                int pColumn = _proposedCell.CellInformation.Column;
                int pRow = _proposedCell.CellInformation.Row;

                SpriteRenderer pChildRenderer = _proposedCell.transform.GetChild(0).GetComponent<SpriteRenderer>();

                //Display the selection sprites depending on whether the propsed to the left, right, above or below the selected
                if (sColumn == pColumn && pRow == sRow + 1)
                    ChangeSelectionSprite(sChildRenderer, pChildRenderer, 3, 4);
                else if (sColumn == pColumn && pRow == sRow - 1)
                    ChangeSelectionSprite(sChildRenderer, pChildRenderer, 4, 3);
                else if (sRow == pRow && pColumn == sColumn - 1)
                    ChangeSelectionSprite(sChildRenderer, pChildRenderer, 2, 1);
                else if (sRow == pRow && pColumn == sColumn + 1)
                    ChangeSelectionSprite(sChildRenderer, pChildRenderer, 1, 2);
                else
                {
                    _proposedCell = null;
                    sChildRenderer.GetComponent<SpriteRenderer>().sprite = _selectionSprites[0];
                }
            }
        }        
    }

    void ChangeSelectionSprite(SpriteRenderer sRenderer, SpriteRenderer pRenderer, int s, int p)
    {
        sRenderer.sprite = _selectionSprites[s];
        pRenderer.sprite = _selectionSprites[p];
    }

    protected override void CheckCompletion()
    {
        //For each row in the grid, a key will be placed with a color value
        SortedDictionary<int, Color> register = new SortedDictionary<int, Color>();
        bool complete = true;
        foreach (ShuffleCellObject.ShuffleCell cell in _cells)
        {
            if (cell.StaticCell || !cell.GObject.activeSelf) continue;

            //If the color of the current cell is not the same as the value held under the row key then the puzzle is not complete
            if (register.ContainsKey(cell.Row))
            {
                if (register[cell.Row] != cell.CurrentColor) complete = false;
            }
            //If there is no key registered for the row then add one
            else register.Add(cell.Row, cell.CurrentColor);
        }

        if (complete)
        {
            SetComplete(Application.streamingAssetsPath + "/ShuffleScores.txt", '-');
        }
    }
}
