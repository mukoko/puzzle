﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;

public class ShufflePuzzleMaker : PuzzleMaker {
    //E-Window Fields
    private int _shuffleLowerBound;
    private int _shuffleUpperBound;
    private int _staticUpperBound;
    private Sprite _blockedSprite;
    private Sprite _normalSprite;

    public Color[] _colors = new Color[7];

    private List<ShuffleCellObject.ShuffleCell> _cells = new List<ShuffleCellObject.ShuffleCell>(81);
    private List<Color> _randomisedColors = new List<Color>(7);

    private int _numberOfShuffles;

    [MenuItem("Tools/Shuffle Maker")]
    public static void ShowWindow()
    {
        GetWindow<ShufflePuzzleMaker>();
    }


    //Initialise the variables that are not declared in the parent class
    protected override void InitialiseOtherVariables()
    {
        _shuffleLowerBound = EditorGUILayout.IntField("Shuffle Lower Bound", _shuffleLowerBound);
        _shuffleUpperBound = EditorGUILayout.IntField("Shuffle Upper Bound", _shuffleUpperBound);
        _staticUpperBound = EditorGUILayout.IntField("Static Upper Bound", _staticUpperBound);
        _normalSprite = (Sprite)EditorGUILayout.ObjectField("Normal Sprite", _normalSprite, typeof(Sprite), true);
        _blockedSprite = (Sprite)EditorGUILayout.ObjectField("Blocked Sprite", _blockedSprite, typeof(Sprite), true);

        _colors = new Color[] { new Color(.382f, .666f, 1f), new Color(.968f, 1, .269f), new Color(.868f, 0, .249f),
        new Color(0f, .632f, .504f), new Color(.915f, .462f, .462f), new Color(.497f, .373f, 1f), new Color(.745f, .457f, .046f) };
    }

    protected override void PuzzleGeneration()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Randomize Colors"))
            RandomizeColors();

        if (GUILayout.Button("Shuffle Colors"))
            ShuffleColors();
        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// Retrieves the relevant information that needs to be saved to the shuffle puzzle file
    /// </summary>
    protected override void AddToFile()
    {
        string path = Application.dataPath + "/Resources/ShufflePuzzles.txt";
        StringBuilder stringBuilder = new StringBuilder();

        string cellArrayString = "[";
        string rowColorString = "[";
        string cellColorAssign = "[";

        foreach (Transform child in _gridObject.transform)
        {
            //CellColorIndex - the color id of the iterated cell
            //Cell type - whether the cell is not active (0), active (1) or static/blocked (2)
            int cellColorIndex = 0;
            int cellType = 0;

            if (child.gameObject.activeSelf)
            {
                ShuffleCellObject.ShuffleCell shuffleCell = child.GetComponent<ShuffleCellObject>().CellInformation;
                GetCellInfo(shuffleCell, ref cellType, ref cellColorIndex);
            }

            cellArrayString += cellType.ToString();
            cellColorAssign += cellColorIndex.ToString();
        }

        //Stores each color, in its randomised order
        foreach (Color color in _randomisedColors)
        {
            rowColorString += color.ToString() + ":";
        }

        int lastid = 0;
        //Retrieves the ID for this new puzzle
        GetID(path, ref lastid);

        stringBuilder.Append(lastid.ToString() + "-" + _numberOfShuffles + " ");
        stringBuilder.Append(cellArrayString + "] ");
        stringBuilder.Append(cellColorAssign + "] ");
        stringBuilder.Append(rowColorString + "]");

        //Appends puzzle to the ShufflePuzzle.txt file
        FileAppendPuzzle(path, stringBuilder);
    }

    /// <summary>
    /// Returns the cell type (active or static/blocked) and the color id of a cell
    /// </summary>
    void GetCellInfo(ShuffleCellObject.ShuffleCell shuffleCell, ref int type, ref int colorIndex)
    {
        if (shuffleCell.StaticCell)
            type = 2;
        else
        {
            type = 1;
            colorIndex = _randomisedColors.IndexOf(shuffleCell.CurrentColor) + 1;
        }
    }

    /// <summary>
    /// Resets the grid holder object to its default
    /// </summary>
    protected override void Clear()
    {
        foreach (Transform child in _gridObject.transform)
        {
            child.gameObject.SetActive(true);
            SpriteRenderer renderer = child.GetComponent<SpriteRenderer>();
            renderer.color = Color.white;
            renderer.sprite = _normalSprite;

            ShuffleCellObject.ShuffleCell shuffleCell = child.GetComponent<ShuffleCellObject>().CellInformation;
            shuffleCell.CurrentColor = Color.white;
            shuffleCell.StaticCell = false;
        }
    }

    /// <summary>
    /// Randomizes the order of the _colors array, tnen assigns the rows colors to this new order.
    /// </summary>
    void RandomizeColors()
    {
        _cells.Clear();
        _randomisedColors.Clear();
        _randomisedColors.AddRange(_colors);

        //Swaps the color at position n with another at random location k
        int n = _randomisedColors.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n);
            Color s = _randomisedColors[k];
            _randomisedColors[k] = _randomisedColors[n];
            _randomisedColors[n] = s;
        }

        //If the cell object is active, then assign the new color to it
        foreach (Transform child in _gridObject.transform)
        {
            if (child.gameObject.activeSelf)
            {
                ShuffleCellObject.ShuffleCell shuffleCell = child.GetComponent<ShuffleCellObject>().CellInformation;
                _cells.Add(shuffleCell);

                shuffleCell.StaticCell = false;
                SpriteRenderer renderer = child.GetComponent<SpriteRenderer>();
                renderer.sprite = _normalSprite;
                renderer.color = _randomisedColors[shuffleCell.Row - 1];

                shuffleCell.CurrentColor = renderer.color;
            }
        }
    }

    /// <summary>
    /// This is what generates the random puzzle.
    /// In summary, it chooses a random cell and swaps the color with a valid neighbour
    /// </summary>
    void ShuffleColors()
    {
        if (_cells.Count == 0) return;

        //The number of times the generator should attempt to swap a pair of cells
        int numberOfShuffles = Random.Range(_shuffleLowerBound, _shuffleUpperBound + 1);
        _numberOfShuffles = numberOfShuffles;

        //Number of static cells left to place
        int remainingStaticAmt = _staticUpperBound;

        List<ShuffleCellObject.ShuffleCell> staticCells = new List<ShuffleCellObject.ShuffleCell>(_staticUpperBound);

        while (remainingStaticAmt > 0)
        {
            //Randomly turns an active cell to a static/blocked cell
            AddRandomStaticCell(ref staticCells);
            remainingStaticAmt--;
        }

        List<ShuffleCellObject.ShuffleCell> shuffableCells = new List<ShuffleCellObject.ShuffleCell>(_cells.Count);
        shuffableCells.AddRange(_cells.FindAll(c => !c.StaticCell));

        while (numberOfShuffles > 0)
        {
            //Randomly swaps a non static cell with another neighbouring cell
            
            //Retrieve a list of all the valid cells
            List<ShuffleCellObject.ShuffleCell> validCells = ValidMovement(shuffableCells);
            if (validCells.Count == 0) break;

            //Attempt to swap one of the valid cells with another
            //If true the swap was successful
            if (RandomlySwapCellColors(validCells, shuffableCells)) numberOfShuffles--;
        }
    }

    /// <summary>
    /// Randomly turn an active cell to a static one
    /// </summary>
    void AddRandomStaticCell(ref List<ShuffleCellObject.ShuffleCell> staticCells)
    {
        ShuffleCellObject.ShuffleCell randomCell = _cells[Random.Range(0, _cells.Count)];
        //Do not allow duplicates
        //However will still count as an attempt
        if (staticCells.Contains(randomCell)) return;

        randomCell.StaticCell = true;
        SpriteRenderer renderer = randomCell.GObject.GetComponent<SpriteRenderer>();
        renderer.color = Color.white;
        renderer.sprite = _blockedSprite;
        staticCells.Add(randomCell);
    }

    /// <summary>
    /// Picks a random valid cell and a valid neighbour and swaps their colors
    /// </summary>
    bool RandomlySwapCellColors(List<ShuffleCellObject.ShuffleCell> validCells, List<ShuffleCellObject.ShuffleCell> shuffableCells)
    {   
        ShuffleCellObject.ShuffleCell toSend = validCells[Random.Range(0, validCells.Count)];

        List<ShuffleCellObject.ShuffleCell> neighbours = GetValidNeighbours(toSend, shuffableCells);
        ShuffleCellObject.ShuffleCell toReceive = neighbours[Random.Range(0, neighbours.Count)];

        if (toSend != null && toReceive != null)
        {
            Color cA = toSend.CurrentColor;
            Color cB = toReceive.CurrentColor;

            toSend.CurrentColor = cB;
            toSend.GObject.GetComponent<SpriteRenderer>().color = cB;

            toReceive.CurrentColor = cA;
            toReceive.GObject.GetComponent<SpriteRenderer>().color = cA;
            return true;
        }

        return false;
    }

    //From the current cell, find all the adjacent cells that are active and non static
    List<ShuffleCellObject.ShuffleCell> GetValidNeighbours(ShuffleCellObject.ShuffleCell cell, List<ShuffleCellObject.ShuffleCell> shuffableCells)
    {
        List<ShuffleCellObject.ShuffleCell> validNeighbours = new List<ShuffleCellObject.ShuffleCell>();

        ShuffleCellObject.ShuffleCell sCell = shuffableCells.Find(c => c.Column == cell.Column - 1 && c.Row == cell.Row);
        if (CheckNeighbour(cell, sCell)) validNeighbours.Add(sCell);

        sCell = shuffableCells.Find(c => c.Column == cell.Column + 1 && c.Row == cell.Row);
        if (CheckNeighbour(cell, sCell)) validNeighbours.Add(sCell);

        sCell = shuffableCells.Find(c => c.Column == cell.Column && c.Row == cell.Row - 1);
        if (CheckNeighbour(cell, sCell)) validNeighbours.Add(sCell);

        sCell = shuffableCells.Find(c => c.Column == cell.Column && c.Row == cell.Row + 1);
        if (CheckNeighbour(cell, sCell)) validNeighbours.Add(sCell);

        return validNeighbours;
    }
    
    //Retrieves all of the cells that have a valid neighbour, (in boundary and non static)
    List<ShuffleCellObject.ShuffleCell> ValidMovement(List<ShuffleCellObject.ShuffleCell> shuffableCells)
    {
        List<ShuffleCellObject.ShuffleCell> validCells = new List<ShuffleCellObject.ShuffleCell>();

        foreach (ShuffleCellObject.ShuffleCell cell in shuffableCells)
            if (ValidNeighbours(cell)) validCells.Add(cell);        

        return validCells;
    }

    private bool ValidNeighbours(ShuffleCellObject.ShuffleCell cell)
    {
        int validNeighbours = 0;

        if (CheckNeighbour(cell, cell.Column - 1, cell.Row))
            validNeighbours++;

        if (CheckNeighbour(cell, cell.Column + 1, cell.Row))
            validNeighbours++;

        if (CheckNeighbour(cell, cell.Column, cell.Row - 1))
            validNeighbours++;

        if (CheckNeighbour(cell, cell.Column, cell.Row + 1))
            validNeighbours++;

        return validNeighbours > 0;
    }


    private bool CheckNeighbour(ShuffleCellObject.ShuffleCell a, ShuffleCellObject.ShuffleCell b)
    {
        if (b != null && a != null)
        {
            if (a.CurrentColor != b.CurrentColor)
                return true;
        }

        return false;
    }

    private bool CheckNeighbour(ShuffleCellObject.ShuffleCell cell, int column, int row)
    {
        ShuffleCellObject.ShuffleCell sCell = _cells.Find(c => c.Column == column && c.Row == row);
        if (sCell != null)
        {
            if (sCell.CurrentColor != cell.CurrentColor && !sCell.StaticCell)
                return true;
        }

        return false;
    }
}
