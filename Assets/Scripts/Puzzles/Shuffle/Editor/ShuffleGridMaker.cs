﻿using UnityEngine;
using UnityEditor;

public class ShuffleGridMaker : GridMaker {

    //Opens the Window
    [MenuItem("Tools/Shuffle Grid Maker")]
    public static void ShowWindow()
    {
        GetWindow<ShuffleGridMaker>("Shuffle Grid Maker");
    }

    //Called by each cell created by the grid maker
    protected override void InitialiseCell(GameObject obj, int column, int row)
    {
        obj.GetComponent<ShuffleCellObject>().Initialise(column, row);
    }
}
