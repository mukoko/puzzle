﻿using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

public class BlockManager : PuzzleManager {

    [SerializeField]
    private Color[] _colors;

    [SerializeField]
    private Sprite _cellSprite;

    [SerializeField]
    private Sprite _cellHighlighter;

    private List<GameObject> _miniBlocks;

    private List<BlockCellObject.BlockCell> _cells;
    private List<Block> _blocks;

    private HashSet<BlockCellObject> _overlap = new HashSet<BlockCellObject>();

    private Block _selectedBlock;

    /// <summary>
    /// Holds information on each block that makes up the puzzle
    /// </summary>
    private class Block : MonoBehaviour
    {
        public int id;
        public float x, y;
        public List<GameObject> cells;
        public Vector3 centerSum = Vector3.zero;
        public HashSet<BlockCellObject> overlapping = new HashSet<BlockCellObject>();
        public CellLocation centerCell;
        public void Initialise(int id, float x, float y) { this.id = id; this.x = x; this.y = y; cells = new List<GameObject>(); }

    }

    private class CellLocation : MonoBehaviour
    {
        public void Initialise(int x, int y) { oX = x; oY = y; }
        public int oX, oY;
    }

    protected override void Initialise(int puzzleID)
    {
        base.Initialise(puzzleID);

        _cells = new List<BlockCellObject.BlockCell>();
        _blocks = new List<Block>();

        CreateBlocks();
        SetCellToBlocks();
        SetBlockPositions();
    }

    /// <summary>
    /// The block info is stored in the puzzle file entry
    /// </summary>
    void CreateBlocks()
    {
        int indexOfFirstComma = _puzzleInformation[2].IndexOf(",");
        int blockCount = 0;
        for (int i = 0; i < _puzzleInformation[2].Length; i++)
        {
            char c = _puzzleInformation[2][i];
            //Each block is separated by a delimiter ','
            if (c.Equals(','))
            {
                blockCount++;
                GameObject holder = new GameObject();
                string s = null;
                if (i == indexOfFirstComma)
                    s = _puzzleInformation[2].Substring(1, i - 1);
                else
                {
                    string sub = _puzzleInformation[2].Substring(0, i - 1);
                    int prevComma = sub.LastIndexOf(",");

                    s = _puzzleInformation[2].Substring(prevComma + 1, i - 1 - prevComma);
                }

                //The position of the block is retrieved
                float x = float.Parse(s.Substring(0, s.IndexOf(":")));
                float y = float.Parse(s.Substring(s.IndexOf(":") + 1));

                //Block is created
                Block b = holder.AddComponent<Block>();
                _blocks.Add(b);
                b.Initialise(blockCount, x, y);
            }
        }
    }

    /// <summary>
    /// Finds the block that each cell belongs to found in the puzzle file.
    /// Initialise the cell and add it to the block
    /// </summary>
    void SetCellToBlocks()
    {
        int currentPosition = 0;
        foreach (Transform child in _gridObject.transform)
        {
            //Check if grid is active
            currentPosition++;
            if (int.Parse(_puzzleInformation[0][currentPosition].ToString()) == 0)
            {
                child.gameObject.SetActive(false);
                continue;
            }

            BlockCellObject.BlockCell cell = child.GetComponent<BlockCellObject>().CellInformation;
            _cells.Add(cell);

            //Get the id of the block which the cell belongs
            int blockID = int.Parse(_puzzleInformation[1][currentPosition].ToString());

            //Create a copy of the cell to be placed into the mini block
            GameObject copy = Instantiate(cell.GObject);
            copy.AddComponent<CellLocation>().Initialise(cell.Column, cell.Row);
            Destroy(copy.GetComponent<BlockCellObject>());

            SpriteRenderer renderer = copy.GetComponent<SpriteRenderer>();
            renderer.sprite = _cellSprite;
            renderer.color = _colors[blockID - 1];
            renderer.sortingOrder = 2;

           // BoxCollider2D collider = copy.AddComponent<BoxCollider2D>();
            //Vector2 S = renderer.sprite.bounds.size;
            //collider.size = S;
            
            //Add the cell to the block
            Block b = GetBlock(blockID);

            if (b != null)
            {
                b.cells.Add(copy);
                copy.transform.SetParent(b.transform);
                b.centerSum = new Vector3(b.centerSum.x + copy.transform.position.x, b.centerSum.y + copy.transform.position.y);
            }
        }
    }

    /// <summary>
    /// Set the position of every block, find the block nearest to the center and set it as the Block's center position
    /// </summary>
    void SetBlockPositions()
    {
        foreach (Block b in _blocks)
        {
            b.centerSum = new Vector3(b.centerSum.x / b.cells.Count, b.centerSum.y / b.cells.Count);

            GameObject closest = b.cells[0];
            foreach (GameObject obj in b.cells)
                if (Vector3.Distance(b.centerSum, obj.transform.position) < Vector3.Distance(b.centerSum, closest.transform.position))
                    closest = obj;

            b.centerSum = closest.transform.position;
            b.centerCell = closest.GetComponent<CellLocation>();
            b.transform.DetachChildren();
            b.transform.position = b.centerSum;

            foreach (GameObject obj in b.cells)
                obj.transform.SetParent(b.transform);

            b.transform.localScale = new Vector3(0.4f, 0.4f);
            b.transform.position = new Vector3(b.x, b.y);
        }
    }

    protected override void GetPuzzle()
    {
        TextAsset file = Resources.Load("BlockPuzzles") as TextAsset;
        string[] lines = file.text.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

        string puzzleInfo = GetPuzzleInformation(lines, _puzzleID, '-');

        if (puzzleInfo != null)
        {
            int indexOfFirstSpace = puzzleInfo.IndexOf(" ");
            int indexOfSecondSpace = puzzleInfo.IndexOf(" ", puzzleInfo.IndexOf(" ") + 1);
            int indexOfThirdSpace = puzzleInfo.LastIndexOf(" ");

            _puzzleInformation = new string[3];
            _puzzleInformation[0] = puzzleInfo.Substring(indexOfFirstSpace + 1, indexOfSecondSpace - 1 - indexOfFirstSpace);
            _puzzleInformation[1] = puzzleInfo.Substring(indexOfSecondSpace + 1, indexOfThirdSpace - 1 - indexOfSecondSpace);
            _puzzleInformation[2] = puzzleInfo.Substring(indexOfThirdSpace + 1);
        }
    }

    Block GetBlock(int id)
    {
        return _blocks.Find(b => b.id == id);
    }

    // Update is called once per frame
    protected override void Update ()
    {
        if (_completed) return;
        base.Update();
        
        if (Input.GetMouseButtonDown(0))
        {
            //When you click the mouse button, checks if the mouse is over a block if so it is selected
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D collidingObject = Physics2D.OverlapPoint(mousePosition);

            if (collidingObject != null)
            {
                Transform rootTransform = collidingObject.transform.root;
                Block block = rootTransform.GetComponent<Block>();
                if (block != null)
                {
                    SelectBlock(block, mousePosition);
                }
            }
        }

        if (_selectedBlock != null)
        {
            Vector3 mouseToViewPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);

            //If the mouse position goes out of the view then release the block
            if (mouseToViewPosition.x > 1 || mouseToViewPosition.x < 0 || mouseToViewPosition.y > 1 || mouseToViewPosition.y < 0)
                ForceRelease();

            //At this point the _selectBlock may be set to null, so we check again
            if (_selectedBlock != null)
            {
                Vector3 mouseToScreenPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (mouseToScreenPosition != _selectedBlock.transform.position)
                {
                    //Move the block to the mouse position
                    _selectedBlock.transform.position = new Vector3(mouseToScreenPosition.x, mouseToScreenPosition.y);

                    Collider2D[] collidingObject = Physics2D.OverlapPointAll(mouseToScreenPosition);
                    Collider2D centerBlock = collidingObject.FirstOrDefault(c => c.GetComponent<BlockCellObject>() != null);

                    //Remove the current highlighted hover cells
                    ClearOverlap();
                    if (centerBlock != null)
                        DisplayBlockHover(centerBlock); 
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (_overlap.Count == _selectedBlock.cells.Count)
                        DrawBlockOnCells();
                    else ForceRelease();
                }
            }
        }

        //Shoot a if over a cell highlight the cells the block will be placed on.
	}
    
    /// <summary>
    /// When the MouseButton(0) has been pressed over a block
    /// </summary>
    void SelectBlock(Block block, Vector2 mousePosition)
    {
        //Set block size to normal size
        block.transform.localScale = new Vector3(1f, 1f);
        block.transform.position = new Vector3(mousePosition.x, mousePosition.y);

        //Display selected block infront of others
        foreach (Transform child in block.transform)
            child.GetComponent<SpriteRenderer>().sortingOrder++;

        //Any grid cell that the block had occupied is reset
        foreach (BlockCellObject c in block.overlapping)
            c.CellInformation.Occupied = false;
        block.overlapping.Clear();

        _selectedBlock = block;
    }

    void DrawBlockOnCells()
    {
        foreach (BlockCellObject cell in _overlap)
        {
            cell.CellInformation.Occupied = true;
            _selectedBlock.overlapping.Add(cell);
        }

        foreach (GameObject obj in _selectedBlock.cells)
            obj.GetComponent<SpriteRenderer>().sortingOrder--;

        //ElementAt 0 will always be the center
        _selectedBlock.transform.position = _overlap.ElementAt(0).transform.position;
        ClearOverlap();
        _selectedBlock = null;

        CheckCompletion();
    }

    void DisplayBlockHover(Collider2D centerBlock)
    {
        BlockCellObject bC = centerBlock.GetComponent<BlockCellObject>();
        if (_cells.Contains(bC.CellInformation) && !bC.CellInformation.Occupied)
        {
            _overlap.Add(centerBlock.GetComponent<BlockCellObject>());

            //Iterates through each cell in the selected
            //Checks if it is over a cell that isn't occupied, if so adds it to overlap
            foreach (GameObject obj in _selectedBlock.cells)
            {
                if (obj == _selectedBlock.centerCell.gameObject) continue;

                Collider2D[] collidingObject = Physics2D.OverlapPointAll(obj.transform.position);
                centerBlock = collidingObject.FirstOrDefault(c => c.GetComponent<BlockCellObject>() != null);

                if (centerBlock == null) break;

                bC = centerBlock.GetComponent<BlockCellObject>();
                if (_cells.Contains(bC.CellInformation) && !bC.CellInformation.Occupied)
                    _overlap.Add(centerBlock.GetComponent<BlockCellObject>());
            }

            //If all the cells in the Block are in _overlap that means it can be drawing there, so the cells in the grid are highlighted
            if (_overlap.Count == _selectedBlock.cells.Count)
            {
                Color c = _colors[_selectedBlock.id - 1];
                foreach (BlockCellObject cell in _overlap)
                {
                    SpriteRenderer renderer = cell.transform.GetChild(0).GetComponent<SpriteRenderer>();
                    renderer.sprite = _cellHighlighter;
                    renderer.color = new Color(c.r, c.g, c.b, 0.67f);
                }
            }
        }
    }

    /// <summary>
    /// Clears the highlight showing the cells that a block is hovering over
    /// </summary>
    void ClearOverlap()
    {
        foreach (BlockCellObject cell in _overlap)
        {
            SpriteRenderer renderer = cell.transform.GetChild(0).GetComponent<SpriteRenderer>();
            renderer.sprite = null;
            renderer.color = Color.white;
        }

        _overlap.Clear();
    }

    /// <summary>
    /// Sets the block back to its original mini object
    /// </summary>
    void ForceRelease()
    {
        _selectedBlock.transform.localScale = new Vector3(0.4f, 0.4f);
        _selectedBlock.transform.position = new Vector3(_selectedBlock.x, _selectedBlock.y);
        foreach (Transform child in _selectedBlock.transform)
            child.GetComponent<SpriteRenderer>().sortingOrder--;

        ClearOverlap();
        _selectedBlock = null;
    }

    protected override void CheckCompletion()
    {
        bool complete = true;
        //Every cell needs to be occupied for the puzzle to be complete
        foreach (BlockCellObject.BlockCell cell in _cells)
        {
            if (!cell.Occupied)
            {
                complete = false;
                break;
            }
        }

        if (complete)
            SetComplete(Application.streamingAssetsPath + "/BlockScores.txt", '-');        
    }
}
