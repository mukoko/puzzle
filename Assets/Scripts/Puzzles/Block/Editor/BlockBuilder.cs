﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is part of the Block Puzzle Generator.
/// In summary the class uses an algorithm which randomly walks through different cells in the grid to make up block pieces. 
/// </summary>
public class BlockBuilder
{
    /// <summary>
    /// Stores information on the cells that form up the block
    /// </summary>
    public class Block
    {
        public List<BlockCellObject.BlockCell> Cells { get; private set; }
        public int MaxCellCount { get; private set; }
        public Color Color { get; set; }
        public GameObject MiniObject { get; set; }


        public Block(int count)
        {
            MaxCellCount = count;
            Cells = new List<BlockCellObject.BlockCell>();
            Color = Color.white;
        }
    }

    private List<BlockCellObject.BlockCell> _cells;
    private BlockCellObject.BlockCell _currentCell;

    public List<Block> Blocks { get; private set; }

    private Block _currentBlock;

    private int _lowerConjoinBound;
    private int _upperConjoinBound;

    private bool _complete;

    public BlockBuilder(List<BlockCellObject.BlockCell> cells, int lcb, int ucb)
    {
        _cells = cells;
        _lowerConjoinBound = lcb;
        _upperConjoinBound = ucb;
        Blocks = new List<Block>();

        //Sets a random start point for the block walk algo
        int startIndex = Random.Range(0, _cells.Count);
        _currentCell = _cells[startIndex];
        //Sets the point to visited so it cannot be walked on again
        _currentCell.Visited = true;

        //Creates the first block and adds the start cell to the block
        _currentBlock = new Block(Random.Range(_lowerConjoinBound, _upperConjoinBound + 1));
        _currentBlock.Cells.Add(_currentCell);
        _currentCell.Block = _currentBlock;

        while (!_complete) Conjoin();
    }

    /// <summary>
    /// While the current block has a valid neighbour, and the max block count permits another cell to be added to the block
    /// then add an adjacent neighbour to the block
    /// </summary>
    private void Conjoin()
    {
        //Only all join if there is a valid neighbour and max. count permits so
        if (RouteAvaliable(_currentCell) && _currentBlock.Cells.Count < _currentBlock.MaxCellCount)
        {
            bool foundCell = false;
            while (!foundCell)
            {
                int direction = Random.Range(1, 5);
                int x = _currentCell.Column;
                int y = _currentCell.Row;

                //Pick a random neighbour that has not been visited
                if (direction == 1 && CellExistsnNotVisited(x, y - 1))
                {
                    y--;
                    foundCell = true;
                }
                else if (direction == 2 && CellExistsnNotVisited(x, y + 1))
                {
                    y++;
                    foundCell = true;
                }
                else if (direction == 3 && CellExistsnNotVisited(x + 1, y))
                {
                    x++;
                    foundCell = true;
                }
                else if (direction == 4 && CellExistsnNotVisited(x - 1, y))
                {
                    x--;
                    foundCell = true;
                }

                //Now mark that cell as the current visited cell and add it to the block
                //This repeats until no valid neighbour is found or the max. capacity for a block is reached
                if (foundCell)
                {
                    _currentCell = _cells.Find(c => c.Column == x && c.Row == y);
                    _currentCell.Visited = true;
                    _currentBlock.Cells.Add(_currentCell);
                    _currentCell.Block = _currentBlock;
                }
            }
        }
        else
        {
            //Else create a new block and find a new start point for the next block start point
            Blocks.Add(_currentBlock);
            _currentBlock = new Block(Random.Range(_lowerConjoinBound, _upperConjoinBound + 1));
            Search();
        }
    }

    private void Search()
    {
        _complete = true;
        foreach (BlockCellObject.BlockCell cell in _cells)
        {
            //Find a cell that is unvisited but has a visited neighbour as the start point for the next block search cycle
            if (!cell.Visited && AdjacentCellVisited(cell))
            {
                _complete = false;
                _currentCell = cell;
                _currentCell.Visited = true;
                _currentBlock.Cells.Add(_currentCell);
                _currentCell.Block = _currentBlock;
                return;
            }
        }
    }

    /// <summary>
    /// Returns true if atleast one adjacent cell has been visited
    /// </summary>
    private bool AdjacentCellVisited(BlockCellObject.BlockCell cell)
    {
        int visitedCells = 0;

        BlockCellObject.BlockCell bCell = GetCell(cell.Column - 1, cell.Row);
        if (bCell != null)
            if (bCell.Visited) visitedCells++;

        bCell = GetCell(cell.Column + 1, cell.Row);
        if (bCell != null)
            if (bCell.Visited) visitedCells++;

        bCell = GetCell(cell.Column, cell.Row - 1);
        if (bCell != null)
            if (bCell.Visited) visitedCells++;

        bCell = GetCell(cell.Column + 1, cell.Row + 1);
        if (bCell != null)
            if (bCell.Visited) visitedCells++;

        return visitedCells > 0;
    }

    /// <summary>
    /// Returns true if atleast one adjacent cell has not been visited
    /// </summary>
    private bool RouteAvaliable(BlockCellObject.BlockCell cell)
    {
        int avaliableRoutes = 0;

        if (CellExistsnNotVisited(cell.Column - 1, cell.Row))
            avaliableRoutes++;
        if (CellExistsnNotVisited(cell.Column + 1, cell.Row))
            avaliableRoutes++;
        if (CellExistsnNotVisited(cell.Column, cell.Row + 1))
            avaliableRoutes++;
        if (CellExistsnNotVisited(cell.Column, cell.Row - 1))
            avaliableRoutes++;

        return avaliableRoutes > 0;
    }

    private BlockCellObject.BlockCell GetCell(int column, int row)
    {
        return _cells.Find(c => c.Column == column && c.Row == row);
    }

    private bool CellExistsnNotVisited(int column, int row)
    {
        BlockCellObject.BlockCell bC = GetCell(column, row);
        if (bC == null) return false;

        return !bC.Visited;
    }
}

