﻿using UnityEngine;
using UnityEditor;

public class BlockGridMaker : GridMaker {
    private Sprite _blank0;
    private Sprite _blank1;

    //Opens the Window
    [MenuItem("Tools/Block Grid Maker")]
    public static void ShowWindow()
    {
        GetWindow<BlockGridMaker>("Block Grid Maker");
    }

    //Initialise additional variables that are not found in the parent class
    protected override void InitialiseOtherVariables()
    {
        _blank0 = (Sprite)EditorGUILayout.ObjectField("Sprite 0", _blank0, typeof(Sprite), true);
        _blank1 = (Sprite)EditorGUILayout.ObjectField("Sprite 1", _blank1, typeof(Sprite), true);
    }

    //Called by each cell created by the grid maker
    protected override void InitialiseCell(GameObject obj, int column, int row)
    {
        //Draw alternative cells are different color. x o x o x o
        //Example                                     o x o x x o
        SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
        if (row % 2 == 0)
        {
            if (column % 2 == 0) renderer.sprite = _blank0;
            else renderer.sprite = _blank1;
        }
        else
        {
            if (column % 2 == 0) renderer.sprite = _blank1;
            else renderer.sprite = _blank0;
        }

        obj.GetComponent<BlockCellObject>().Initialise(column, row);
    }
}
