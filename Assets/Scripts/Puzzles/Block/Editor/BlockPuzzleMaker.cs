﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class BlockPuzzleMaker : PuzzleMaker {

    private int _upperBlockCount;
    private int _lowerCellJoinCount;
    private int _upperCellJoinCount;
    private Sprite _miniSprite;

    private List<BlockCellObject.BlockCell> _cells;
    private List<GameObject> _miniBlockHolder = new List<GameObject>();

    private BlockBuilder _blockBuilder;

    [MenuItem("Tools/Block Maker")]
    public static void ShowWindow()
    {
        GetWindow<BlockPuzzleMaker>();
    }

    /// <summary>
    /// Destroys all the block objects
    /// </summary>
    protected override void Clear()
    {
        foreach (GameObject obj in _miniBlockHolder)        
            DestroyImmediate(obj);

        _miniBlockHolder.Clear();
    }

    /// <summary>
    /// Retrieves the necessary information that is required to re-create the puzzle at runtime
    /// </summary>
    protected override void AddToFile()
    {
        string path = Application.dataPath + "/Resources/BlockPuzzles.txt";

        string blockCount = _blockBuilder.Blocks.Count.ToString();
        string cellArrayString = "[";
        string cellBlockNoArrayString = "[";
        string blockPosArrayString = "[";

        foreach (Transform cellObject in _gridObject.transform)
        {
            int cellBlockIndex = 0;
            
            //If the cell is active set arrayIndex to 1 else 0
            //Also find the index of block that the cell belongs to, so we can group the cells together
            if (cellObject.gameObject.activeSelf)
            {
                cellArrayString += 1.ToString();
                BlockBuilder.Block cellBlock = (BlockBuilder.Block)cellObject.GetComponent<BlockCellObject>().CellInformation.Block;
                cellBlockIndex = _blockBuilder.Blocks.IndexOf(cellBlock) + 1;

            }
            else cellArrayString += 0.ToString();
            cellBlockNoArrayString += cellBlockIndex.ToString();
        }

        //Get the position of the miniblock, so we do not need to re-position then at runtime
        foreach (BlockBuilder.Block b in _blockBuilder.Blocks)
        {
            blockPosArrayString += b.MiniObject.transform.position.x.ToString("#.##") + ":" + b.MiniObject.transform.position.y.ToString("#.##") + ",";
        }

        //Retrieve the new id the this puzzle
        int lastid = 0;
        GetID(path, ref lastid);

        //Add the necessary info to a sBuilder then append it to the file
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(lastid.ToString() + "-" + blockCount + " ");
        stringBuilder.Append(cellArrayString + "] ");
        stringBuilder.Append(cellBlockNoArrayString + "] ");
        stringBuilder.Append(blockPosArrayString);

        FileAppendPuzzle(path, stringBuilder);
    }

    protected override void InitialiseOtherVariables()
    {
        _upperBlockCount = EditorGUILayout.IntField("Upper Block Count", _upperBlockCount);
        _lowerCellJoinCount = EditorGUILayout.IntField("Lower Cell Join Count", _lowerCellJoinCount);
        _upperCellJoinCount = EditorGUILayout.IntField("Upper Cell Join Count", _upperCellJoinCount);
        _miniSprite = (Sprite)EditorGUILayout.ObjectField("Minigrid Cell Sprite", _miniSprite, typeof(Sprite), true);
    }

    protected override void PuzzleGeneration()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Generate Block"))
            GenerateBlocks();
        GUILayout.EndHorizontal();
    }

    void GenerateBlocks()
    {
        Clear();

        //Initialise Cells list
        _cells = new List<BlockCellObject.BlockCell>();
        foreach (Transform child in _gridObject.transform)
        {
            if (child.gameObject.activeSelf)
            {
                BlockCellObject.BlockCell bCell = child.GetComponent<BlockCellObject>().CellInformation;
                _cells.Add(bCell);
                bCell.Visited = false;
                bCell.Block = null;
            }
        }

        //Creates the blocks through the algorithm written in the BlockBuilder class
        _blockBuilder = new BlockBuilder(_cells, _lowerCellJoinCount, _upperCellJoinCount);

        //Finds that blocks that have a cell count below the minimum and joins them with others
        //Also continues to join blocks if the block count is above the maximum
        JoinBlocks();

        //For each block, a mini verison of it is created, which are used as the pieces used to play the game
        CreateMiniBlocks();
       
        //Set all of the mini blocks in a random puzzle at the bottom of the view
        foreach (GameObject gameObject in _miniBlockHolder)
        {
            gameObject.transform.position = new Vector3(Random.Range(-0.45f, 0.42f), Random.Range(.475f, 0.1f));
        }
    }

    /// <summary>
    /// Create the mini version of each block piece
    /// </summary>
    void CreateMiniBlocks()
    {
        foreach (BlockBuilder.Block block in _blockBuilder.Blocks)
        {
            block.Color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
            GameObject blockHolder = new GameObject();
            List<GameObject> copies = new List<GameObject>();
            Vector3 centerSum = new Vector3(0, 0);
            foreach (BlockCellObject.BlockCell bCell in block.Cells)
                CreateCopy(bCell, blockHolder, ref copies, ref centerSum);

            blockHolder.transform.DetachChildren();

            centerSum = new Vector3(centerSum.x / block.Cells.Count, centerSum.y / block.Cells.Count);
            blockHolder.transform.position = centerSum;

            foreach (GameObject obj in copies)
            {
                obj.transform.SetParent(blockHolder.transform);
                SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
                renderer.color = block.Color;
                renderer.sprite = _miniSprite;
            }

            blockHolder.transform.localScale = new Vector3(0.4f, 0.4f);
            block.MiniObject = blockHolder;
            _miniBlockHolder.Add(blockHolder);
        }
    }

    /// <summary>
    /// Creates a copy of all the cells in a block, 
    /// For the copy set the parent to a new gameobject
    /// The center sum will be the position of the new gamobject, we want to be at the center of all the blocks
    /// </summary>
    void CreateCopy(BlockCellObject.BlockCell blockCell, GameObject blockHolder, ref List<GameObject> copies, ref Vector3 centerSum)
    {
        GameObject copy = Instantiate(blockCell.GObject);
        copy.transform.SetParent(blockHolder.transform);
        copies.Add(copy);
        centerSum = new Vector3(centerSum.x + copy.transform.position.x, centerSum.y + copy.transform.position.y);
    }

    /// <summary>
    /// Joins blocks together to meet the minimum cell count & the maximum block count requirements
    /// </summary>
    void JoinBlocks()
    {
        //There may be blocks that do not meet the minimum block count requirement, if so join them with other adjacent blocks           
        for (int bIndex = _blockBuilder.Blocks.Count - 1; bIndex >= 0; bIndex--)
        {
            BlockBuilder.Block block = _blockBuilder.Blocks[bIndex];
            if (block.Cells.Count >= _lowerCellJoinCount) continue;
            JoinToAdjacent(block);
        }

        //If the block count is above the maximum, then join the smallest block to a neighbour
        while (_blockBuilder.Blocks.Count > _upperBlockCount)
        {
            _blockBuilder.Blocks.OrderBy(o => o.Cells.Count);
            BlockBuilder.Block smallest = _blockBuilder.Blocks[0];
            JoinToAdjacent(smallest);
        }
    }

    /// <summary>
    /// Takes a block and finds all of its adjacent blocks, then joins the cells in the given block to the cells in a random adjacent one
    /// </summary>
    void JoinToAdjacent(BlockBuilder.Block block)
    {
        List<BlockBuilder.Block> adjacentBlocks = new List<BlockBuilder.Block>();
        foreach (BlockCellObject.BlockCell cell in block.Cells)
            GetAdjacentBlocks(cell, ref adjacentBlocks);

        BlockBuilder.Block newBlock = adjacentBlocks[Random.Range(0, adjacentBlocks.Count)];
        foreach (BlockCellObject.BlockCell cell in block.Cells)
        {
            newBlock.Cells.Add(cell);
            cell.Block = newBlock;
        }

        _blockBuilder.Blocks.Remove(block);
    }

    /// <summary>
    /// Finds all the cells that are adjacent to the parameter cell, if the adjacent cells belong to another block then it is added
    /// </summary>
    void GetAdjacentBlocks(BlockCellObject.BlockCell cell, ref List<BlockBuilder.Block> adjacentBlocks)
    {
        AddAdjacentBlock(GetCell(cell.Column - 1, cell.Row), cell, ref adjacentBlocks);
        AddAdjacentBlock(GetCell(cell.Column + 1, cell.Row), cell, ref adjacentBlocks);
        AddAdjacentBlock(GetCell(cell.Column, cell.Row - 1), cell, ref adjacentBlocks);
        AddAdjacentBlock(GetCell(cell.Column, cell.Row + 1), cell, ref adjacentBlocks);
    }

    void AddAdjacentBlock(BlockCellObject.BlockCell adjacentCell, BlockCellObject.BlockCell cell, ref List<BlockBuilder.Block> adjacentBlocks)
    {
        if (adjacentCell != null)
            if (adjacentCell.Block != cell.Block && !adjacentBlocks.Contains((BlockBuilder.Block)adjacentCell.Block)) adjacentBlocks.Add((BlockBuilder.Block)adjacentCell.Block);
    }

    BlockCellObject.BlockCell GetCell(int column, int row)
    {
        return _cells.Find(c => c.Column == column && c.Row == row);
    }
}
