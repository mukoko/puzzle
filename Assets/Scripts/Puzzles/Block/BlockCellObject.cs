﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BlockCellObject : MonoBehaviour {
    [SerializeField]
    private BlockCell _blockCell;

    public BlockCell CellInformation { get { return _blockCell; } }
    public void Initialise(int column, int row)
    {
        _blockCell = new BlockCell(column, row, gameObject);
    }

    [System.Serializable]
    public class BlockCell
    {
        [SerializeField]
        private int _column;

        [SerializeField]
        private int _row;

        [SerializeField]
        private GameObject _gObj;

        public int Column { get { return _column; } }
        public int Row { get { return _row; } }
        public GameObject GObject { get { return _gObj; } }
        public bool Visited { get; set; }
        public object Block { get; set; }
        public bool Occupied { get; set; }

        public BlockCell(int column, int row, GameObject obj = null)
        {
            _column = column;
            _row = row;
            _gObj = obj;
        }
    }
}
