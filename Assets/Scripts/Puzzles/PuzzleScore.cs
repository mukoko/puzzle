﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Score
{
    public int level;
    public string time;

    public Score(int level, string time)
    {
        this.level = level;
        this.time = time;
    }
}

[System.Serializable]
public class ScoreFile
{
    public List<Score> puzzleScores = new List<Score>();
}

public class PuzzleScore {

    [DllImport("__Internal")]
    private static extern void SyncFiles();

    public static void Save(PuzzleType puzzleType, int level, string minutes, string seconds, ref Text recordTime)
    {
        string dataPath = Application.persistentDataPath + "/" + puzzleType.ToString() + "Scores.dat";
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream fileStream;
        ScoreFile scoreFile = new ScoreFile();

        recordTime.text = "Record Time : " + minutes + ":" + seconds;

        if (!File.Exists(dataPath))
        {
            fileStream = File.Create(dataPath);
            scoreFile.puzzleScores.Add(new Score(level, minutes + ":" + seconds));
        }
        else
        {
            List<Score> currentScores = Load(puzzleType);

            if (currentScores == null) scoreFile.puzzleScores.Add(new Score(level, minutes + ":" + seconds));
            else scoreFile.puzzleScores.AddRange(currentScores);

            File.WriteAllText(dataPath, string.Empty);
            fileStream = File.Open(dataPath, FileMode.Open);

            bool hasLevel = false;
            for(int i = 0; i < scoreFile.puzzleScores.Count; i++)
            {
                Score score = scoreFile.puzzleScores[i];
                if (score.level == level)
                {
                    hasLevel = true;

                    recordTime.text = "Record Time : " + score.time.Substring(0, 2) + ":" + score.time.Substring(3);
                    if (int.Parse(minutes) <= int.Parse(score.time.Substring(0, 2)))
                    {
                        if (int.Parse(seconds) < int.Parse(score.time.Substring(3)))
                        {
                            score.time = minutes + ":" + seconds;
                            recordTime.text = "Record Time : " + minutes + ":" + seconds;
                        }
                    }
                    break;
                }
            }

            if (!hasLevel) scoreFile.puzzleScores.Add(new Score(level, minutes + ":" + seconds));
        }

        binaryFormatter.Serialize(fileStream, scoreFile);
        fileStream.Close();

        if (Application.platform == RuntimePlatform.WebGLPlayer)
            SyncFiles();
    }

    public static List<Score> Load(PuzzleType puzzleType)
    {
        string dataPath = Application.persistentDataPath + "/" + puzzleType.ToString() + "Scores.dat";


        if (File.Exists(dataPath))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = File.Open(dataPath, FileMode.Open);

            if (fileStream.Length == 0)
            {
                fileStream.Close();
                return null;
            }

            ScoreFile puzzleScores = (ScoreFile)binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            return puzzleScores.puzzleScores;
        }

        return null;
    }
    
}
