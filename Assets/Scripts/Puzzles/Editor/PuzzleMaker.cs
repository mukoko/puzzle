﻿using System.IO;
using System.Text;
using UnityEngine;
using UnityEditor;

public abstract class PuzzleMaker : EditorWindow {
    protected GameObject _gridObject;

    protected virtual void OnGUI()
    {
        GUILayout.Label("Create Puzzle");
        GUILayout.Space(1f);

        _gridObject = (GameObject)EditorGUILayout.ObjectField("Grid Object", _gridObject, typeof(GameObject), true);

        InitialiseOtherVariables();
        PuzzleGeneration();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add To File"))
            AddToFile();

        if (GUILayout.Button("Clear"))
            Clear();
        GUILayout.EndHorizontal();
    }

    //Iterates through a puzzle file, and finds the last valid id and increments it by 1
    protected void GetID(string path, ref int lastid)
    {
        StreamReader streamReader = new StreamReader(path);
        string line = streamReader.ReadLine();

        while (line != null)
        {
            lastid++;
            line = streamReader.ReadLine();
        }

        streamReader.Close();
        lastid++;
    }

    //Adds a puzzle's info to the file
    protected void FileAppendPuzzle(string path, StringBuilder stringBuilder)
    {
        StreamWriter streamWriter = new StreamWriter(path, true);
        streamWriter.WriteLine(stringBuilder.ToString());
        streamWriter.Close();
    }

    protected virtual void InitialiseOtherVariables() { }

    protected abstract void PuzzleGeneration();
    protected abstract void AddToFile();
    protected abstract void Clear();
}
