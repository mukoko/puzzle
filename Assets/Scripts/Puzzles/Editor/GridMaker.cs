﻿using UnityEngine;
using UnityEditor;

public abstract class GridMaker : EditorWindow {
    //E-Window Fields
    protected int _rowCount;
    protected int _columnCount;
    protected GameObject _cellObject;


    protected virtual void OnGUI()
    {
        GUILayout.Label("Create Grid");
        GUILayout.Space(1f);

        //E-Window Fields Initialisation
        _rowCount = EditorGUILayout.IntField("Row Count", _rowCount);
        _columnCount = EditorGUILayout.IntField("Column Count", _columnCount);
        _cellObject = (GameObject)EditorGUILayout.ObjectField("Cell Object", _cellObject, typeof(GameObject), true);

        InitialiseOtherVariables();

        //E-Window Buttons Initialisation
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Create Grid"))
            CreateGrid();
        GUILayout.EndHorizontal();
    }

    protected void CreateGrid()
    {
        if (_rowCount <= 0 || _columnCount <= 0)
            return;

        //Iterates from the last cell to the first so that the order that gameobjects are listed in the hierarchy are correct
        for (int row = _rowCount; row >= 1; row--)
        {
            for (int column = _columnCount; column >= 1; column--)
            {
                //Creating the cell object
                GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(_cellObject);
                obj.transform.position = new Vector3((column - 1) * 0.12f, (row - 1) * -(0.12f));
                obj.name = "Grid Cell - (" + column + ", " + row + ")";
                InitialiseCell(obj, column, row);
            }
        }
    }
 
    //Any other fields that need to be declare will be done in the this method.
    protected virtual void InitialiseOtherVariables() { }

    //Every class that inherits from this has its own declaration for a cell object
    protected abstract void InitialiseCell(GameObject obj, int column, int row);
}
