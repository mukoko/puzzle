﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : SimpleMenu<MainMenu> {

    //Each puzzle button on the main menu should have an instance of PuzzleButton class.
    [System.Serializable]
    private struct PuzzleButton
    {
        [SerializeField]
        private GameObject _gameObject;

        [SerializeField]
        private PuzzleType _puzzleType;

        public Button button { get { return _gameObject.GetComponent<Button>(); } }
        public Text completeText { get { return _gameObject.transform.GetChild(1).GetComponent<Text>(); } }
        public PuzzleType puzzleType { get { return _puzzleType; } }
    }


    [SerializeField]
    private List<PuzzleButton> _puzzleButtons;

    protected override void Awake()
    {
        base.Awake();

        foreach (PuzzleButton puzzleButton in _puzzleButtons)
        {
            //Get the number of puzzles for each type of puzzle
            //Done by counting the lines in the Puzzle File.
            TextAsset textAsset = Resources.Load(puzzleButton.puzzleType + "Puzzles") as TextAsset;
            int puzzleCount = textAsset.text.Split(new char[] { '\n', '\r' }, System.StringSplitOptions.RemoveEmptyEntries).Length;

            int completionCount = 0;

            List<Score> scores = PuzzleScore.Load(puzzleButton.puzzleType);
            if (scores != null) completionCount = scores.Count;

            puzzleButton.completeText.text = completionCount + "/" + puzzleCount;
            puzzleButton.button.onClick.AddListener(delegate { PuzzleSelectMenu.Show(puzzleButton.puzzleType); } );

        }
    }

}
