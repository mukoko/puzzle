﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleSelectMenu : Menu<PuzzleSelectMenu> {

    [SerializeField]
    private Text _puzzleTypeText;

    [SerializeField]
    private GameObject _selectionButton;

    [SerializeField]
    private GameObject _cheatPanel;

    [SerializeField]
    private Color[] _difficultyColors = new Color[5];
    
    private int _puzzleCount;
    private static PuzzleType _puzzleType;

    private List<Score> _scores;

    public static void Show(PuzzleType puzzleType)
    {
        _puzzleType = puzzleType;
        Open();
    }

    protected override void Awake()
    {
        base.Awake();

        _puzzleTypeText.text = _puzzleType.ToString();

        //Get the number of puzzles for the given type
        TextAsset textAsset = Resources.Load(_puzzleType.ToString() + "Puzzles") as TextAsset;
        _puzzleCount = textAsset.text.Split(new char[] { '\n', '\r' }, System.StringSplitOptions.RemoveEmptyEntries).Length;

        _scores = PuzzleScore.Load(_puzzleType);

        for (int i = 1; i <= _puzzleCount; i++)
        {
            GameObject entry = Instantiate(_selectionButton);
            entry.SetActive(true);

            Test t = new Test(i);
            entry.transform.GetChild(0).GetComponent<Text>().text = i.ToString();

            //You can only select a puzzle that is either the first puzzle or the dictionary contains a score for the previous puzzle.
            //ie, you can only do puzzle 4 once you've completed puzzle 3.
            if (i == 1 || HasScore(i - 1))
            {
                entry.GetComponent<Button>().onClick.AddListener(delegate { EntryPressed(t); });
                Image image = entry.GetComponent<Image>();

                if (i <= 50) image.color = _difficultyColors[0];
                else if (i <= 100) image.color = _difficultyColors[1];
                else if (i <= 150) image.color = _difficultyColors[2];
                else if (i <= 200) image.color = _difficultyColors[3];
                else image.color = _difficultyColors[4];
            }
            else entry.GetComponent<Button>().enabled = false;


            if (HasScore(i))
                entry.transform.GetChild(1).GetComponent<Text>().text = GetScore(i);

            entry.transform.SetParent(_selectionButton.transform.parent, false);
        }
    }

    bool HasScore(int i)
    {
        if (_scores == null) return false;

        foreach (Score score in _scores)
            if (score.level == i) return true;

        return false;
    }

    string GetScore(int i)
    {
        if (_scores == null) return null;

        foreach (Score score in _scores)
            if (score.level == i) return score.time;

        return null;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            _cheatPanel.SetActive(!_cheatPanel.activeSelf);
    }

    public void CheatLoad(string input)
    {
        int n;
        bool isNumeric = int.TryParse(input, out n);

        if (isNumeric)
        {
            if (n <= _puzzleCount)
                EntryPressed(n);
        }
    }

    //For some reason Delegeate wont let me pass an int.
    private class Test
    {
        public int i;
        public Test(int i)
        {
            this.i = i;
        }
    }

    void EntryPressed(int i)
    {
        PlayerPrefs.SetInt(_puzzleType.ToString() + "PuzzleID", i);
        SceneManager.LoadScene((int) _puzzleType);
    }

    void EntryPressed(Test i)
    {
        PlayerPrefs.SetInt(_puzzleType.ToString() + "PuzzleID", i.i);
        SceneManager.LoadScene((int) _puzzleType);
    }
}
